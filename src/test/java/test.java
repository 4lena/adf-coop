import com.Adf.LoanSystem.LoanSystemApplication;
import com.Adf.LoanSystem.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LoanSystemApplication.class)
public class test {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithMockUser // Simulate an authenticated user
    public void testSignUp() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/SignUp")
                        .param("nid", "1111100000")
                        .param("fname", "testUser")
                        .param("lname", "testUser")
                        .param("phone", "testUser")
                        .param("password", "testPassword"))
                .andExpect(MockMvcResultMatchers.status().isOk()); // Assuming registration redirects to login

        assertNotNull(userRepository.findByNid(1111100000));
    }

    /*@Test
    @WithMockUser
    public void testSignIn() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/SignIn")
                        .param("nid", "1111100000")
                        .param("password", "testPassword"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }*/

    @Test
    @WithMockUser(username = "1111100000", password = "testPassword")
    public void testSignIn2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/loanDetails"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "1111100000", password = "testPassword")
    public void testSignOut() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/SignIn?logout"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}