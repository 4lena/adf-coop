FROM openjdk:17
EXPOSE 8082
COPY ./target/*.jar loan-system.jar
ENTRYPOINT ["java", "-jar", "/loan-system.jar"]
